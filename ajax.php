<?
include('db.php');
include('dbhelper.php');

$action = $_GET["action"];
$dbh = new DBHelper();
switch($action){
	case "getMonths":{
		echo json_encode($dbh->getMonths());
		break;
	}
	
	case "getYears":{
		echo json_encode($dbh->getYears());
		break;
	}
	
	case "getReport":{
		$year = $_GET["year"];
		$month = $_GET["month"];
		$user_type=$_GET["user_type"];
		echo json_encode($dbh->getReport($month,$year,$user_type));
		break;
	}
}



?>