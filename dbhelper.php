<?
require_once("db.php");
class DBHelper{
	protected $pdo;
	public function __construct(){
		$this->pdo = DB::getPDO();
	}
	
	// возвращает месяцы в которые есть записи
	public function getMonths(){
		$STH = $this->pdo->prepare('SELECT DISTINCT MONTH(DATA) AS "MONTH" FROM payments ORDER BY 1 ASC');
		$STH->execute();
		$months = array();
		while($month = $STH->fetch(PDO::FETCH_ASSOC)){
			$months[]=$month["MONTH"];
		}
		return $months;		
	}
	
	// возвращает годы в которые есть записи
	public function getYears(){
		
		$STH = $this->pdo->prepare('SELECT DISTINCT YEAR(DATA) AS "YEAR" FROM payments ORDER BY 1 ASC');
		$STH->execute();
		$years = array();
		while($year = $STH->fetch(PDO::FETCH_ASSOC)){
			$years[]=$year["YEAR"];
		}
		return $years;	
	}
	
	// формируем отчет
	public function getReport($month,$year,$user_type){
		// приводим к int чтобы обезопасить данные
		$month = (int)$month;
		$year = (int)$year;
		$where = ' 1=1 ';
		if ($user_type!="ALL"){
			$user_type=(int)$user_type;
			$where =' cl.TYPE='.$user_type.' OR cl.TYPE IS NULL ';
		}
			
		$STH = $this->pdo->prepare('
			SELECT 
				serv1.NAME AS "SERVICE"
				,SUM(distinct a1.s1) AS "BALANCE_ST"
				,SUM(distinct a1.s2) AS "OUT"
				,SUM(distinct a1.s3) AS "IN"
				,SUM(distinct a1.s4) AS "RECALCULATE"
				,SUM(distinct a1.s5) AS "ITOGO"

			FROM
				services serv1
				LEFT JOIN (
					SELECT serv.name,pay1.summa AS s1,pay2.summa AS s2 ,pay3.summa AS s3, pay4.summa AS s4, pay5.summa AS s5 
					FROM
						services serv
						LEFT JOIN payments pay1
							ON serv.ID = pay1.ACNT_ID 
							AND pay1.DATA<"'.$year.'-'.$month.'-01"
						LEFT JOIN payments pay2
							ON serv.ID = pay2.ACNT_ID 
							AND pay2.SUMMA<0
							AND MONTH(pay2.DATA)=:month
							AND YEAR(pay2.DATA)=:year	
						LEFT JOIN payments pay3
							ON serv.ID = pay3.ACNT_ID 
							AND pay3.SUMMA>0
							AND MONTH(pay3.DATA)=:month
							AND YEAR(pay3.DATA)=:year
						LEFT JOIN payments pay4
							ON serv.ID = pay4.ACNT_ID 
							AND MONTH(pay4.DATA)=:month
							AND YEAR(pay4.DATA)=:year
							AND pay4.PAY_ID=3
						LEFT JOIN payments pay5
							ON serv.ID = pay5.ACNT_ID 
							AND pay5.DATA< DATE_ADD("'.$year.'-'.$month.'-1", INTERVAL 1 MONTH)
						LEFT JOIN clients cl 
							ON cl.ID=pay5.CLIENT_ID
							OR cl.ID=pay1.CLIENT_ID
							OR cl.ID=pay2.CLIENT_ID
							OR cl.ID=pay3.CLIENT_ID
							OR cl.ID=pay4.CLIENT_ID
					WHERE '.$where.'
				) a1
			ON a1.name=serv1.name
			GROUP BY 1
		');
		$STH->bindValue("month", $month, \PDO::PARAM_INT);
		$STH->bindValue("year", $year, \PDO::PARAM_INT);
		$STH->execute();
		$rows = array();
		while($row = $STH->fetch(PDO::FETCH_ASSOC)){
			//обнуляем отсутствующие значения
			foreach($row as $k=>$v){
				if($v==NULL){
					$row[$k]=0;
				}
			}
			$rows[]=$row;
		}
		return $rows;
		
	}
	
	
}