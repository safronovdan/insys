CREATE TABLE IF NOT EXISTS clients (
	`ID` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`NAME` VARCHAR(255),
	`TYPE` INT
);

CREATE TABLE IF NOT EXISTS services (
	`ID` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`NAME` VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS payments_types (
	`ID` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`NAME` VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS payments (
	`ID` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`CLIENT_ID` INT,
	`SUMMA` FLOAT(13,2),
	`DATA` DATE,
	`DESCRIPTION` TEXT,
	`ACNT_ID` INT,
	`PAY_ID` INT,
	FOREIGN KEY (ACNT_ID) REFERENCES services(ID),
	FOREIGN KEY (PAY_ID) REFERENCES payments_types(ID),
	FOREIGN KEY (CLIENT_ID) REFERENCES clients(ID)
	
);

