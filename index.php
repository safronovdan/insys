<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

</head>
<body>
<div class="s-form">
Месяц и год для отчета:
<select id="s-months">
</select>.<select id="s-years">
</select>

</div>
<div class="s-form">
Выберите тип клиентов:
<select id="s_user_type">
	<option value="ALL">Все</option>
	<option value="0">Физ. лица</option>
	<option value="1">юр. лица</option>
</select>
</div>

<div class="report" id="ajaxcont">
</div>
<input type="button" value ="REPORT" onclick="getReport(); return false;"></input>

</body>
</html>

<script>
function getReport(){
	var month = $("#s-months").val();
	var year = $("#s-years").val();
	var user_type=$("#s_user_type").val();
	var rows = {};
	$.ajax({
	  type: "GET",
	  url: "ajax.php",
	  data: {"action":"getReport",'year':year,'month':month,'user_type':user_type},
	  success: function(data){
		rows = $.parseJSON( data );
		var report_table = $('<table id="report_table" class="table table-bordered"><tr><th>Услуга</th><th>Баланс на начало месяца</th><th>Приход</th><th>Расход</th><th>Перерасчет</th><th>Итого</th></tr></table>');
				
		$(rows).each(function(indx, element){
		  report_table.append('<tr><td>'+element.SERVICE+'</td><td>'+element.BALANCE_ST+'</td><td>'+element.IN+'</td><td>'+element.OUT+'</td><td>'+element.RECALCULATE+'</td><td>'+element.ITOGO+'</td></tr>');
		});
		$("#ajaxcont").html(report_table);
	  },
	  error: function(data){
		  console.log( "ERR: " + data );
	  }
	});
	
	
}
$(document).ready(function(){
	getMonths();
	getYears();
});

function getMonths(){
	var months = {}
	$.ajax({
	  type: "GET",
	  url: "ajax.php",
	  data: {"action":"getMonths"},
	  success: function(data){
		months = $.parseJSON( data );	
		var sel = $("#s-months");
		$(months).each(function(indx, element){
		  sel.append('<option value="'+element+'">'+element+'</option>');
		});
		
	  },
	  error: function(data){
		  console.log( "ERR: " + data );
	  }
	});
}

function getYears(){
	var years = {}
	$.ajax({
	  type: "GET",
	  url: "ajax.php",
	  data: {"action":"getYears"},
	  success: function(data){
		years = $.parseJSON( data );
		var sel = $("#s-years");
		$(years).each(function(indx, element){
		  sel.append('<option value="'+element+'">'+element+'</option>');
		});
		
	  },
	  error: function(data){
		  console.log( "ERR: " + data );
	  }
	});
}

</script>

<style>
.s-form{
	padding: 5px;
}
.report{max-width: 90%;}

</style>